package pl.akademiakodu.PlannerOnline.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.PlannerOnline.model.Task;
import pl.akademiakodu.PlannerOnline.model.TaskState;

import java.util.List;

public interface TaskRepository  extends CrudRepository<Task, Long> {
    List<Task> findByTaskState(TaskState taskState);
}
