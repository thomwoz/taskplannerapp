package pl.akademiakodu.PlannerOnline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlannerOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlannerOnlineApplication.class, args);
	}
}
