package pl.akademiakodu.PlannerOnline.model;

import javax.persistence.*;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String description;

    private TaskState taskState = TaskState.TODO;

    public Task(String description) {
        this.description = description;
    }

    public Task(){}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskState getTaskState() {
        return taskState;
    }

    public void setTaskState(TaskState taskState) {
        this.taskState = taskState;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "NR: " + id +
                "\nOpis: " + description + '\n' +
                "Stan: " + taskState;
    }
}
