package pl.akademiakodu.PlannerOnline.model;

public enum TaskState {
    TODO, INPROGRESS, DONE;
}
