package pl.akademiakodu.PlannerOnline.controller;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.PlannerOnline.model.Task;

@RestController
public class TaskRestController {

    private static final String template = "%s";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/taskjson")
    public Task Task(@RequestParam Long id) {
        return new Task(String.format(template, id));
    }
}