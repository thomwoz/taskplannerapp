package pl.akademiakodu.PlannerOnline.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.akademiakodu.PlannerOnline.dao.TaskRepository;
import pl.akademiakodu.PlannerOnline.model.Task;
import pl.akademiakodu.PlannerOnline.model.TaskState;

@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/")
    public String showIndex(){
        return "index";
    }

    @GetMapping("/addtaskform")
    public String addTaskForm(){
        return "form";
    }

    @PostMapping("/addtask")
    public String addTask(@ModelAttribute Task task, ModelMap modelMap){
        modelMap.addAttribute("task", task);

        taskRepository.save(task);
        return "addtask";
    }

    @GetMapping("/showtasktodo")
    public String showTasksToDo(ModelMap modelMap){
        modelMap.addAttribute("taskstodo", taskRepository.findByTaskState(TaskState.TODO));
        return "showtasktodo";
    }

    @GetMapping("/showtaskinprogress")
    public String showTasksInProgress(ModelMap modelMap){
        modelMap.addAttribute("tasksinprogress", taskRepository.findByTaskState(TaskState.INPROGRESS));
        return "showtasksinprogress";
    }

    @GetMapping("/showtaskdone")
    public String showTasksDone(ModelMap modelMap){
        modelMap.addAttribute("tasksdone", taskRepository.findByTaskState(TaskState.DONE));
        return "showtasksdone";
    }

    @GetMapping("/showall")
    public String showAll(ModelMap modelMap){
        modelMap.addAttribute("taskstodo", taskRepository.findByTaskState(TaskState.TODO));
        modelMap.addAttribute("tasksinprogress", taskRepository.findByTaskState(TaskState.INPROGRESS));
        modelMap.addAttribute("tasksdone", taskRepository.findByTaskState(TaskState.DONE));
        return "showall";
    }

    @GetMapping("/task/delete/{id}")
    public String deleteTask(@PathVariable Long id){
        taskRepository.deleteById(id);
        return "deletedtask";
    }

    @GetMapping("/task/changestateinprogress/{id}")
    public String changeStateInProgress(@PathVariable Long id) {
        Task task = taskRepository.findById(id).get();
        task.setTaskState(TaskState.INPROGRESS);
        taskRepository.save(task);
        return "changedstate";
    }

    @GetMapping("/task/changestatedone/{id}")
    public String changeStateDone(@PathVariable Long id) {
        Task task = taskRepository.findById(id).get();
        task.setTaskState(TaskState.DONE);
        System.out.println("DUOADUOADDSHHDBSHFVGHSDVGHSVDGHSVDGHSVDGHS");
        taskRepository.save(task);
        return "changedstate";
    }





}
